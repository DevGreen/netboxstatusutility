﻿
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Devices.Enumeration;

namespace NetboxStatusUtility
{
    internal class Program
    {
        public static List<BlueToothDevice> _blueToothDevices;
        public static StringBuilder _log;
        public static string _version;
        public static string _portCheckUrl;
        public static int _scanLength;
        static void Main(string[] args)
        {

            _version = "1.1";
            _portCheckUrl = "portquiz.net";
            _scanLength = 10; //in seconds

            _blueToothDevices = new List<BlueToothDevice>();
            _log = new StringBuilder();
            Console.ForegroundColor = ConsoleColor.White;

            MachineInfo();
            NeoRouterTest();
            FlashCashTest();
            GPayTest();
            BlueToothscan();
            SaveLogFile();

            Console.WriteLine("\r\nPress Enter to close the application");
            Console.ReadLine();

        }

        private static void MachineInfo()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            LogString("  =====================");
            LogString("  ||   Machine Info  ||");
            LogString("  =====================");
            LogString("");
            Console.ResetColor();

            List<MachineResults> tableResults = new List<MachineResults>();
            tableResults.Add(new MachineResults
            {
                TestName = "Netbox Status Utility Version",
                Description = $"{_version}",
            });

            tableResults.Add(new MachineResults
            {
                TestName = "Machine Name",
                Description = $"{Environment.MachineName}",
            });
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    tableResults.Add(new MachineResults
                    {
                        TestName = "IP Address",
                        Description = $"{ip.ToString()}",
                    });
                }
            }
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo d in allDrives)
            {
                if (d.IsReady == true)
                {
                    tableResults.Add(new MachineResults
                    {
                        TestName = $"{d.VolumeLabel} Disk Space Available",
                        Description = $"{ConvertBytesToMegabytes(d.AvailableFreeSpace)}/{ConvertBytesToMegabytes(d.TotalSize)} MB",
                    });
                }
            }
            PrettyPrint(tableResults.ToStringTable(
              new[] { "Test", "Description" },
              a => a.TestName, a => a.Description));

        }
 
        private static void NeoRouterTest()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            LogString("  =====================");
            LogString("  ||NeoRouter Service||");
            LogString("  =====================");
            LogString("");
            Console.ResetColor();

            List<TableResults> tableResults = new List<TableResults>();
            List<Service> services = new List<Service>();
            services.Add(new Service { ServiceName = "NRClientService", Description = "NeoRouter Client Service", Required = true });
            tableResults.AddRange(RunServiceTest(services));

            List<Port> ports = new List<Port>();
            ports.Add(new Port { PortNumber = 32976, Description = "VPN", Required = true });
            ports.Add(new Port { PortNumber = 3389, Description = "RDP (Remote Desktop)", Required = true });
            ports.Add(new Port { PortNumber = 80, Description = "HTTP", Required = false });
            ports.Add(new Port { PortNumber = 443, Description = "HTTPs", Required = false });
            ports.Add(new Port { PortNumber = 5985, Description = "PowerShell Remote (HTTP)", Required = false });
            ports.Add(new Port { PortNumber = 5986, Description = "PowerShell Remote (HTTPs)", Required = false });
            tableResults.AddRange(RunPortTest(ports));

            List<Domain> domains = new List<Domain>();
            domains.Add(new Domain { URL = "flashcashonline.com", Required = true });
            domains.Add(new Domain { URL = "nr2.flashcashonline.com", Required = false });
            domains.Add(new Domain { URL = "nr3.flashcashonline.com", Required = false });
            domains.Add(new Domain { URL = "nr4.flashcashonline.com", Required = false });
            domains.Add(new Domain { URL = "nr5.flashcashonline.com", Required = false });
            domains.Add(new Domain { URL = "nr6.flashcashonline.com", Required = false });
            tableResults.AddRange(RunDomainTest(domains));

            PrettyPrint(tableResults.ToStringTable(
              new[] { "Test", "Description", "Status" },
              a => a.TestName, a => a.Description, a => a.Status));

        }

        private static void FlashCashTest()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            LogString("  =====================");
            LogString("  ||FlashCash Service||");
            LogString("  =====================");
            LogString("");
            Console.ResetColor();

            List<TableResults> tableResults = new List<TableResults>();

            List<Service> services = new List<Service>();
            services.Add(new Service { ServiceName = "NetBoxService", Description = "GI FlashNet Box", Required = true });
            tableResults.AddRange(RunServiceTest(services));

            List<Port> ports = new List<Port>();
            ports.Add(new Port { PortNumber = 5671, Description = "AMQP", Required = true });
            ports.Add(new Port { PortNumber = 443, Description = "HTTPs", Required = true });
            ports.Add(new Port { PortNumber = 80, Description = "HTTP", Required = true });
            tableResults.AddRange(RunPortTest(ports));

            List<Domain> domains = new List<Domain>();
            domains.Add(new Domain { URL = "giflashnet.servicebus.windows.net", Port = 5671, Required = true });
            tableResults.AddRange(RunDomainTest(domains));

            PrettyPrint(tableResults.ToStringTable(
              new[] { "Test", "Description", "Status" },
              a => a.TestName, a => a.Description, a => a.Status));
        }
        private static void GPayTest()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            LogString("  =====================");
            LogString("  ||   GPay Service  ||");
            LogString("  =====================");
            LogString("");
            Console.ResetColor();

            List<TableResults> tableResults = new List<TableResults>();

            List<Service> services = new List<Service>();
            services.Add(new Service { ServiceName = "LDCCMainWindowsService", Description = "Greenwald LDCC Service", Required = true });
            tableResults.AddRange(RunServiceTest(services));


            List<Port> ports = new List<Port>();
            ports.Add(new Port { PortNumber = 8883, Description = "MQTT", Required = true });
            ports.Add(new Port { PortNumber = 5671, Description = "AMQP", Required = true });
            ports.Add(new Port { PortNumber = 5672, Description = "AMQP", Required = true });
            ports.Add(new Port { PortNumber = 443, Description = "HTTPs", Required = true });
            ports.Add(new Port { PortNumber = 1833, Description = "MQTT (unencrypted)", Required = false });
            tableResults.AddRange(RunPortTest(ports));

            List<Domain> domains = new List<Domain>();
            domains.Add(new Domain { URL = "iot-hub-greenwald-one.azure-devices.net", Port = 443, Required = true });
            tableResults.AddRange(RunDomainTest(domains));


            PrettyPrint(tableResults.ToStringTable(
              new[] { "Test", "Description", "Status" },
              a => a.TestName, a => a.Description, a => a.Status));
        }
        private static void BlueToothscan()
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            LogString("  =====================");
            LogString("  ||  Bluetooth Scan  ||");
            LogString("  =====================");
            LogString("");
            Console.ResetColor();

            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("  Scanning for GPay (BLE) devices. Please Wait...  ");
            var watcher = new BluetoothLEAdvertisementWatcher();
            watcher.Received += Watcher_Received;
            watcher.Start();
            System.Threading.Thread.Sleep(_scanLength * 1000);
            watcher.Stop();
            LogString("");
            var client = new BluetoothClient();
            Console.Write("  Scanning for FlashCash (Bluetooth) devices. Please Wait... ");
            BluetoothDeviceInfo[] allDevicesInRange = client.DiscoverDevices();
            foreach (var device in allDevicesInRange)
            {
                _blueToothDevices.Add(new BlueToothDevice
                {
                    BluetoothName = device.DeviceName,
                    BlueToothType = "BlueTooth (FlashCash)",
                    Strength = "-"
                });
            };
            LogString("");

            PrettyPrint(NaturalBluetoothSort(_blueToothDevices).ToStringTable(
              new[] { "Device", "Type", "Strength" },
              a => a.BluetoothName, a => a.BlueToothType, a => a.Strength));



        }
        private static void Watcher_Received(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args)
        {
            if (!string.IsNullOrEmpty(args.Advertisement.LocalName))
            {
                if (!_blueToothDevices.Select(x => x.BluetoothName == args.Advertisement.LocalName).Any())
                {
                    _blueToothDevices.Add(new BlueToothDevice
                    {
                        BluetoothName = args.Advertisement.LocalName,
                        BlueToothType = "BLE (GPay)",
                        Strength = $"{args.RawSignalStrengthInDBm} DBm"
                    });

                }
            }
        }

        private static List<TableResults> RunServiceTest(List<Service> services)
        {
            List<TableResults> tableResults = new List<TableResults>();
            foreach (var service in services)
            {
                tableResults.Add(new TableResults
                {
                    TestName = "Service Running",
                    Description = $"{service.Description} ({service.ServiceName})",
                    Status = TestResult(isServiceRunning(service.ServiceName), service.Required)
                });
                tableResults.Add(new TableResults
                {
                    TestName = "Service StartUp Automatic",
                    Description = $"{service.Description} ({service.ServiceName})",
                    Status = TestResult(isServiceStartupAutomatic(service.ServiceName), service.Required)
                });
            }
            return tableResults;
        }

        private static List<TableResults> RunPortTest(List<Port> ports)
        {
            List<TableResults> tableResults = new List<TableResults>();
            foreach (var port in ports)
            {
                tableResults.Add(new TableResults
                {
                    TestName = "Port Open",
                    Description = $"{port.Description} ({port.PortNumber})",
                    Status = TestResult(IsPortOpen(port.PortNumber), port.Required)
                });
            }
            return tableResults;
        }

        private static List<TableResults> RunDomainTest(List<Domain> domains)
        {
            List<TableResults> tableResults = new List<TableResults>();
            foreach (var domain in domains)
            {
                tableResults.Add(new TableResults
                {
                    TestName = "DNS Resolved",
                    Description = domain.URL,
                    Status = TestResult(IsDnsWhiteListed(domain.URL), domain.Required)
                });
            }
            return tableResults;
        }

        private static void SaveLogFile()
        {
            var fileName = $"netbox-status-log-{DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")}.txt";
            System.IO.File.WriteAllText($"{fileName}", _log.ToString());
            Console.Write($"Results saved to: ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write($"{fileName}\r\n");
            Console.ResetColor();
        }
        private static bool IsDnsWhiteListed(string url)
        {
            try
            {
                var ipAddress = System.Net.Dns.GetHostEntry(url);
                if (ipAddress.AddressList.Length > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static bool IsPortOpen(int portNumber)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.Connect(_portCheckUrl, portNumber);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        private static bool isServiceRunning(string serviceName)
        {
            ServiceController sc = new ServiceController(serviceName);
            try
            {
                if (sc.Status == ServiceControllerStatus.Running)
                    return true;

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }
        private static bool isServiceStartupAutomatic(string serviceName)
        {
            ServiceController sc = new ServiceController(serviceName);
            try
            {
                if (sc.StartType == ServiceStartMode.Automatic)
                    return true;

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private static void LogString(string log)
        {
            _log.Append($"{log}\r\n");
            Console.WriteLine(log);
        }

        private static void PrettyPrint(string table)
        {
            _log.Append(table);
            var columns = table.Split('|');
            foreach (var col in columns)
            {
                if (col.Contains("Passed"))
                    Console.ForegroundColor = ConsoleColor.Green;
                if (col.Contains("Failed"))
                    Console.ForegroundColor = ConsoleColor.Red;
                if (col.Contains("Warning"))
                    Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($"{col}");

                Console.ForegroundColor = ConsoleColor.White;
            }
            LogString("");
        }

        private static string TestResult(bool success, bool required)
        {
            if (success)
            {
                return "Passed";
            }
            else
            {
                if (required)
                {
                    return "Failed";
                }
                else
                {
                    return "Warning";
                }
            }
        }
        static int ConvertBytesToMegabytes(long bytes)
        {
            return Convert.ToInt32(Math.Floor((bytes / 1024f) / 1024f));
        }
        public static IEnumerable<BlueToothDevice> NaturalBluetoothSort(IEnumerable<BlueToothDevice> list)
        {
            int maxLen = list.Select(s => s.BluetoothName.Length).Max();
            Func<string, char> PaddingChar = s => char.IsDigit(s[0]) ? ' ' : char.MaxValue;

            return list
                    .Select(s =>
                        new
                        {
                            OrgStr = s,
                            SortStr = Regex.Replace(s.BluetoothName, @"(\d+)|(\D+)", m => m.Value.PadLeft(maxLen, PaddingChar(m.Value)))
                        })
                    .OrderBy(x => x.SortStr)
                    .Select(x => x.OrgStr);
        }

        private class Port
        {
            public int PortNumber { get; set; }
            public string Description { get; set; }
            public bool Required { get; set; }
        }

        private class Service
        {
            public string ServiceName { get; set; }
            public string Description { get; set; }
            public bool Required { get; set; }
        }
        private class Domain
        {
            public string URL { get; set; }
            public int Port { get; set; }
            public bool Required { get; set; }
        }
        private class MachineResults
        {
            public string TestName { get; set; }
            public string Description { get; set; }
        }
        private class TableResults
        {
            public string TestName { get; set; }
            public string Description { get; set; }
            public string Status { get; set; }
        }

        public class BlueToothDevice {
            public string BluetoothName { get; set; }
            public string BlueToothType { get; set; }
            public string Strength { get; set; }

        }

 
    }
}
